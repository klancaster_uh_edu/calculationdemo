json.array!(@growth_calculators) do |growth_calculator|
  json.extract! growth_calculator, :id, :starting_population, :growth_rate, :time_frame
  json.url growth_calculator_url(growth_calculator, format: :json)
end
