# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

build_table = ->
  time_frame =  $("#growth_calculator_time_frame").val()
  growth_rate = $("#growth_calculator_growth_rate").val()
  starting_population = $("#growth_calculator_starting_population").val()
  $.get "/growth_calculators/population",{pop: starting_population,time: time_frame, rate: growth_rate}

$ ->
  $('#growth_calculator_time_frame,#growth_calculator_growth_rate,#growth_calculator_starting_population').bind 'keyup mouseup mousewheel', ->
    build_table()

  build_table()

