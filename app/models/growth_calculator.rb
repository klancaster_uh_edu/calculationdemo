class GrowthCalculator < ActiveRecord::Base


  # Formula for population at a given time is initial_population * exp(rate * time)
  def self.calculate_population_table(starting_population = 0,time_frame = 0,growth_rate = 0)
    Rails.logger.debug "Inputs: #{starting_population} #{time_frame} #{growth_rate}"
    population = []
    (0..time_frame).each do |t|
      population << starting_population * Math.exp(growth_rate * t)
    end
    return population # the word return is optional - could just put population
  end

  def calculate_population_table
    return self.class.calculate_population_table(self.starting_population,self.time_frame,self.growth_rate)
  end
end
