class GrowthCalculatorsController < ApplicationController
  before_action :set_growth_calculator, only: [:show, :edit, :update, :destroy]

  # GET /growth_calculators
  # GET /growth_calculators.json
  def index
    @growth_calculators = GrowthCalculator.all
  end

  # GET /growth_calculators/1
  # GET /growth_calculators/1.json
  def show
    @population_array = @growth_calculator.calculate_population_table
  end

  # GET /growth_calculators/new
  def new
    @growth_calculator = GrowthCalculator.new
  end

  # GET /growth_calculators/1/edit
  def edit
  end

  # POST /growth_calculators
  # POST /growth_calculators.json
  def create
    @growth_calculator = GrowthCalculator.new(growth_calculator_params)

    respond_to do |format|
      if @growth_calculator.save
        format.html { redirect_to @growth_calculator, notice: 'Growth calculator was successfully created.' }
        format.json { render :show, status: :created, location: @growth_calculator }
      else
        format.html { render :new }
        format.json { render json: @growth_calculator.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /growth_calculators/1
  # PATCH/PUT /growth_calculators/1.json
  def update
    respond_to do |format|
      if @growth_calculator.update(growth_calculator_params)
        format.html { redirect_to @growth_calculator, notice: 'Growth calculator was successfully updated.' }
        format.json { render :show, status: :ok, location: @growth_calculator }
      else
        format.html { render :edit }
        format.json { render json: @growth_calculator.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /growth_calculators/1
  # DELETE /growth_calculators/1.json
  def destroy
    @growth_calculator.destroy
    respond_to do |format|
      format.html { redirect_to growth_calculators_url, notice: 'Growth calculator was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def population
    rate = params[:rate]
    time = params[:time]
    initial_population = params[:pop]
    @population_array = GrowthCalculator.calculate_population_table(initial_population.to_i, time.to_i, rate.to_f)
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_growth_calculator
      @growth_calculator = GrowthCalculator.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def growth_calculator_params
      params.require(:growth_calculator).permit(:starting_population, :growth_rate, :time_frame)
    end
end
