require 'test_helper'

class GrowthCalculatorsControllerTest < ActionController::TestCase
  setup do
    @growth_calculator = growth_calculators(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:growth_calculators)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create growth_calculator" do
    assert_difference('GrowthCalculator.count') do
      post :create, growth_calculator: { growth_rate: @growth_calculator.growth_rate, starting_population: @growth_calculator.starting_population, time_frame: @growth_calculator.time_frame }
    end

    assert_redirected_to growth_calculator_path(assigns(:growth_calculator))
  end

  test "should show growth_calculator" do
    get :show, id: @growth_calculator
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @growth_calculator
    assert_response :success
  end

  test "should update growth_calculator" do
    patch :update, id: @growth_calculator, growth_calculator: { growth_rate: @growth_calculator.growth_rate, starting_population: @growth_calculator.starting_population, time_frame: @growth_calculator.time_frame }
    assert_redirected_to growth_calculator_path(assigns(:growth_calculator))
  end

  test "should destroy growth_calculator" do
    assert_difference('GrowthCalculator.count', -1) do
      delete :destroy, id: @growth_calculator
    end

    assert_redirected_to growth_calculators_path
  end
end
