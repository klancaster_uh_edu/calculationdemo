class CreateGrowthCalculators < ActiveRecord::Migration
  def change
    create_table :growth_calculators do |t|
      t.integer :starting_population
      t.float :growth_rate
      t.integer :time_frame

      t.timestamps null: false
    end
  end
end
