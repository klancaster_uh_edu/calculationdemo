# CIS 4339 Example Application

This Rails app demonstrates some approaches that might be taken to create tabular data on the fly without saving it to the database. It uses JQuery and a Javascript page (js.erb) to render the page.